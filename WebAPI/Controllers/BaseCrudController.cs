﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.Extensions.Configuration;
using WebAPI.Common;
using WebAPI.Data;
using WebAPI.Data.Entities;
using WebAPI.Exceptions;

namespace WebAPI.Controllers
{
    /// <summary>
    /// The base controller.
    /// </summary>
    public abstract class BaseCrudController<T, S> : BaseController
        where T : IdentifiableEntity
        where S : BaseSearchCriteria
    {
        public BaseCrudController(AppDbContext db)
            : base(db)
        {
        }

        #region CRUD

        public virtual T GetEntity(int id)
        {
            var result = Get(id, full: true, required: false);
            return result;
        }

        public virtual IList<T> GetAllEntities()
        {
            return _db.Set<T>().ToList();
        }

        public virtual T CreateEntity(T entity)
        {
            ValidateEntityOnCreate(entity);

            ResolveChildEntities(entity);
            var result = _db.Set<T>().Add(entity);
            SaveChanges();
            return result.Entity;
        }

        public virtual void UpdateEntity(int id, T entity)
        {
            ValidateEntityOnUpdate(id, entity);

            T existing = _db.Set<T>().Find(id);
            CheckFoundEntity(existing);

            // get existing child entities from DB, otherwise new entities will be created in database
            ResolveChildEntities(entity);

            // copy fields to existing entity (attach approach doesn't work for child entities)
            UpdateEntityFields(existing, entity);

            SaveChanges();
        }

        public virtual void DeleteEntity(int id)
        {
            T entity = Get(id, full: true, required: true);

            DeleteAssociatedEntities(entity);
            _db.Set<T>().Remove(entity);
            SaveChanges();
        }

        public virtual SearchResult<T> SearchEntities(S criteria)
        {
            Util.CheckSearchCriteria(criteria);

            IQueryable<T> query = IncludeSearchItemNavigationProperties(_db.Set<T>());

            // construct query conditions
            query = ConstructQueryConditions(query, criteria);

            // set total count of filtered records
            var metadata = new Metadata
            {
                PageIndex = criteria.PageIndex,
                PageSize = criteria.PageSize,
                TotalCount = query.Count()
            };
            var result = new SearchResult<T> { Metadata = metadata };

            // adjust SortBy (e.g. navigation properties should be handled by child services)
            var sortBy = ResolveSortBy(criteria.SortBy);

            if (sortBy.Contains(","))
            {
                var fields = sortBy.Split(",");
                foreach (string softByField in fields)
                {
                    criteria.SortBy = softByField;

                    // construct SortBy property selector expression
                    query = AddSortingCondition(query, criteria);
                }
            }
            else
            {
                criteria.SortBy = sortBy;

                // construct SortBy property selector expression
                query = AddSortingCondition(query, criteria);
            }

            // select required page
            if (criteria.PageIndex > 0)
            {
                query = query.Skip(criteria.PageSize * (criteria.PageIndex - 1)).Take(criteria.PageSize);
            }

            // execute query and set result properties
            result.Items = query.ToList();
            return result;
        }

        public int GetCount(S criteria)
        {
            Util.CheckSearchCriteria(criteria);
            IQueryable<T> query = _db.Set<T>();

            // construct query conditions
            query = ConstructQueryConditions(query, criteria);

            return query.Count();
        }

        #endregion

        protected IQueryable<T> Query()
        {
            return _db.Set<T>();
        }

        public IQueryable<T> QueryFull()
        {
            return IncludeNavigationProperties(_db.Set<T>());
        }

        #region Get helpers

        protected T Get(int id, bool full = true, bool required = false)
        {
            Util.ValidateArgumentPositive(id);

            IQueryable<T> query = _db.Set<T>();
            if (full)
            {
                query = IncludeNavigationProperties(query);
            }

            T entity = query.FirstOrDefault(e => e.Id == id);
            if (required)
            {
                CheckFoundEntity(entity);
            }
            return entity;
        }

        public T GetOne(int id)
        {
            return _db.Set<T>().FirstOrDefault(x => x.Id == id);
        }

        public T GetOneRequired(int id)
        {
            var item = _db.Set<T>().FirstOrDefault(x => x.Id == id);
            CheckFoundEntity(item);
            return item;
        }

        public T GetOne(Expression<Func<T, bool>> predicate)
        {
            return _db.Set<T>().FirstOrDefault(predicate);
        }

        public T GetOneRequired(Expression<Func<T, bool>> predicate)
        {
            var item = _db.Set<T>().FirstOrDefault(predicate);
            CheckFoundEntity(item);
            return item;
        }

        #endregion

        #region validation

        protected virtual void ValidateEntityCommon(T entity)
        {
            Util.ValidateArgumentNotNull(entity);
        }
        protected virtual void ValidateEntityOnCreate(T entity)
        {
            ValidateEntityCommon(entity);
        }
        protected virtual void ValidateEntityOnUpdate(int id, T entity)
        {
            ValidateEntityCommon(entity);
            Util.ValidateArgumentPositive(id);
        }

        protected void EnsureEntityExists(int id)
        {
            EnsureEntityExists<T>(id);
        }

        #endregion

        protected virtual IQueryable<T> ConstructQueryConditions(IQueryable<T> query, S criteria)
        {
            return query;
        }

        protected virtual void DeleteAssociatedEntities(T entity)
        {
            // do nothing by default
        }

        protected void DeleteChildEntity<TChild>(TChild entity)
            where TChild : IdentifiableEntity
        {
            if (entity != null)
            {
                _db.Set<TChild>().Remove(entity);
            }
        }

        protected virtual void ResolveChildEntities(T entity)
        {
            // do nothing by default
        }

        protected void ResolveEntities<TEntity>(IList<TEntity> items)
            where TEntity : IdentifiableEntity
        {
            if (items != null)
            {
                for (int i = 0; i < items.Count; i++)
                {
                    items[i] = ResolveChildEntity(items[i]);
                }
            }
        }

        protected TEntity ResolveChildEntity<TEntity>(TEntity entity, long? id = null, bool isRequired = false)
            where TEntity : IdentifiableEntity
        {
            long? entityId = id > 0 ? id : entity?.Id;
            if (entityId == null)
            {
                if (isRequired)
                {
                    throw new ArgumentException();
                }

                return null;
            }

            TEntity child = _db.Set<TEntity>().Find(entityId.Value);
            if (isRequired)
            {
                CheckFoundEntity(child);
            }
            return child;
        }

        protected virtual void UpdateEntityFields(T existing, T newEntity)
        {
            newEntity.Id = existing.Id;
            _db.Entry(existing).CurrentValues.SetValues(newEntity);
        }

        protected virtual IQueryable<T> IncludeNavigationProperties(IQueryable<T> query)
        {
            // by default do not include any child property
            return query;
        }

        protected virtual IQueryable<T> IncludeSearchItemNavigationProperties(IQueryable<T> query)
        {
            // by default do not include any child property
            return query;
        }

        protected virtual string ResolveSortBy(string sortBy)
        {
            // Note: override in child classes to handle sorting by navigation properties
            if (sortBy == null)
            {
                return "Id";
            }

            return sortBy;
        }

        protected virtual IQueryable<T> AddSortingCondition(IQueryable<T> query, S criteria)
        {
            return criteria.SortType == SortType.ASC
                ? query.OrderBy(criteria.SortBy)
                : query.OrderByDescending(criteria.SortBy);
        }

        public void SaveChanges()
        {
            _db.SaveChanges();
        }

    }
}
