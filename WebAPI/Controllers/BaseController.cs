﻿/*
 * Copyright (c) 2019, TopCoder, Inc. All rights reserved.
 */
using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Common;
using WebAPI.Data;
using WebAPI.Data.Entities;
using WebAPI.Exceptions;

namespace WebAPI.Controllers
{
    /// <summary>
    /// The base controller.
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public abstract class BaseController : ControllerBase
    {
        protected readonly AppDbContext _db;

        public BaseController(AppDbContext db)
        {
            _db = db;
        }

        public TEntity GetOne<TEntity>(int id)
            where TEntity : IdentifiableEntity
        {
            return _db.Set<TEntity>().FirstOrDefault(x => x.Id == id);
        }

        public TEntity GetOneRequired<TEntity>(int id)
            where TEntity : IdentifiableEntity
        {
            var item = _db.Set<TEntity>().FirstOrDefault(x => x.Id == id);
            CheckFoundEntity(item);
            return item;
        }

        public TEntity GetOne<TEntity>(Expression<Func<TEntity, bool>> predicate)
            where TEntity : IdentifiableEntity
        {
            return _db.Set<TEntity>().FirstOrDefault(predicate);
        }

        public TEntity GetOneRequired<TEntity>(Expression<Func<TEntity, bool>> predicate)
            where TEntity : IdentifiableEntity
        {
            var item = _db.Set<TEntity>().FirstOrDefault(predicate);
            CheckFoundEntity(item);
            return item;
        }

        protected void CheckFoundEntity<T>(T entity)
            where T : class
        {
            if (entity == null)
            {
                throw new EntityNotFoundException();
            }
        }

        protected void EnsureEntityExists<TEntity>(int id)
            where TEntity : IdentifiableEntity
        {
            bool exists = _db.Set<TEntity>().Any(x => x.Id == id);
            if (!exists)
            {
                throw new EntityNotFoundException($"Entity with id '{id}' doesn't exist.");
            }
        }
    }
}
