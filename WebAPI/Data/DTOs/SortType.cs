﻿namespace WebAPI.Data.Entities
{
    /// <summary>
    /// An enum that represents sort type.
    /// </summary>
    public enum SortType
    {
        /// <summary>
        /// Represents ascending sort type.
        /// </summary>
        ASC,

        /// <summary>
        /// Represents descending sort type.
        /// </summary>
        DESC
    }
}
