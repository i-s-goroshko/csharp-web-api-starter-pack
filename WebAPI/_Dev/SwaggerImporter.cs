﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebAPI.Swagger;

namespace WebAPI
{
    internal class SwaggerImporter
    {
        internal void Import(ImportSwaggerModel model)
        {
            string contents = File.ReadAllText(model.FilePath);

            var settings = new JsonSerializerSettings();
            settings.MetadataPropertyHandling = MetadataPropertyHandling.Ignore;

            var root = JsonConvert.DeserializeObject<JObject>(contents, settings);

            var entityNames = ImportEntities(model, root);

            ImportDbContext(model, entityNames);

            ImportControllers(model, root);
        }

        private static IList<string> ImportEntities(ImportSwaggerModel model, JObject root)
        {
            var entityNames = new List<string>();

            // read entities
            var components = new List<SwaggerComponent>();
            foreach (var property in (root["components"]["schemas"] as JObject).Properties())
            {
                var component = property.Value.ToObject<SwaggerComponent>();
                var name = property.Name;
                component.Name = char.ToUpper(name[0]) + name.Substring(1);
                components.Add(component);
            }

            // create entities
            EnsureDirCreated(model.EntitiesPath);
            foreach (var item in components)
            {
                if (item.Enum == null)
                {
                    entityNames.Add(item.Name);
                }

                item.CreateEntity(model.EntitiesPath, "WebAPI.Data.Entities");
            }

            return entityNames;
        }

        private static void ImportDbContext(ImportSwaggerModel model, IList<string> entityNames)
        {
            try
            {
                EnsureDirCreated(model.DbContextPath);
                string filePath = Path.Combine(model.ControllersPath, "DataContext.cs");
                using (var sw = new StreamWriter(filePath))
                {
                    sw.WriteLine("using Microsoft.EntityFrameworkCore;");
                    sw.WriteLine("using System;");
                    sw.WriteLine("using System.Linq;");
                    sw.WriteLine("using System.Collections.Generic;");
                    sw.WriteLine("using WebAPI.Data.Entities;");

                    sw.WriteLine();
                    sw.WriteLine("namespace WebAPI.Data");
                    sw.WriteLine("{");
                    sw.WriteLine($"    public class DataContext : DbContext");
                    sw.WriteLine("    {");

                    // DB sets
                    foreach (var item in entityNames)
                    {
                        var plural = ToPlural(item);
                        sw.WriteLine($"        public DbSet<{item}> {plural} {{ get; set; }}");
                    }

                    // ctor
                    sw.WriteLine($"        public DataContext(DbContextOptions<DataContext> contextOptions)");
                    sw.WriteLine("             : base(contextOptions)");
                    sw.WriteLine("        {");
                    sw.WriteLine("        }");

                    sw.WriteLine("    }");
                    sw.WriteLine("}");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static void ImportControllers(ImportSwaggerModel model, JObject root)
        {
            var paths = root["paths"];
            var dict = new Dictionary<string, IList<SwaggerPath>>();
            foreach (var property in (paths as JObject).Properties())
            {
                string controllerName = property.Name.Trim('/');
                if (!string.IsNullOrWhiteSpace(model.ApiPrefix) && controllerName.StartsWith(model.ApiPrefix))
                {
                    controllerName = controllerName.Substring(model.ApiPrefix.Length).TrimStart('/');
                }
                string path = controllerName;
                int index = controllerName.IndexOf('/');
                if (index > 0)
                {
                    controllerName = controllerName.Substring(0, index);
                }

                path = path.Substring(controllerName.Length);

                IList<SwaggerPath> coll;
                if (!dict.TryGetValue(controllerName, out coll))
                {
                    coll = new List<SwaggerPath>();
                    dict[controllerName] = coll;
                }

                var swaggerPath = property.Value.ToObject<SwaggerPath>();
                swaggerPath.Path = path;
                coll.Add(swaggerPath);
            }

            // create entities
            EnsureDirCreated(model.ControllersPath);
            foreach (var path in dict)
            {
                try
                {
                    string entityName = char.ToUpper(path.Key[0]) + path.Key.Substring(1);
                    string controllerName = ToPlural(entityName) + "Controller";
                    string filePath = Path.Combine(model.ControllersPath, controllerName + ".cs");
                    using (var sw = new StreamWriter(filePath))
                    {
                        sw.WriteLine("using Microsoft.AspNetCore.Mvc;");
                        sw.WriteLine("using System;");
                        sw.WriteLine("using System.Linq;");
                        sw.WriteLine("using System.Collections.Generic;");
                        sw.WriteLine("using WebAPI.Data;");
                        sw.WriteLine("using WebAPI.Data.Entities;");
                        sw.WriteLine("using WebAPI.Exceptions;");
                        sw.WriteLine("using WebAPI.Common;");
                        sw.WriteLine();
                        sw.WriteLine("namespace WebAPI.Controllers");
                        sw.WriteLine("{");
                        sw.WriteLine($"    public class {controllerName} : BaseCrudController<{entityName}, BaseSearchCriteria>");
                        sw.WriteLine("    {");
                        // ctor
                        sw.WriteLine($"        public {controllerName}(AppDbContext db)");
                        sw.WriteLine("             : base(db)");
                        sw.WriteLine("        {");
                        sw.WriteLine("        }");

                        foreach (var endpoint in path.Value)
                        {
                            endpoint.WriteMethods(sw);
                        }

                        sw.WriteLine("    }");
                        sw.WriteLine("}");
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        private static void EnsureDirCreated(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        private static string ToPlural(string item)
        {
            string plural;
            if (item.EndsWith("s"))
            {
                plural = item + "es";
            }
            else if (item.EndsWith("y"))
            {
                plural = item.Substring(0, item.Length - 1) + "ies";
            }
            else
            {
                plural = item + "s";
            }

            return plural;
        }
    }
}
