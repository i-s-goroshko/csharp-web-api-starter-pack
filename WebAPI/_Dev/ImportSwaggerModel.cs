﻿namespace WebAPI
{
    public class ImportSwaggerModel
    {
        public string FilePath { get; set; }
        public string ApiPrefix { get; set; }
        public string ControllersPath { get; set; }
        public string EntitiesPath { get; set; }
        public string DbContextPath { get; set; }
    }
}
