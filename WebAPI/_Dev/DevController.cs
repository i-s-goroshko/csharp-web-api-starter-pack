﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data;
using WebAPI.Data.Entities;

namespace WebAPI.Controllers
{
    // TODO: This is temporary controller for development purposes
    [AllowAnonymous]
    public class DevController : BaseController
    {
        public DevController(AppDbContext db)
            : base(db)
        {
        }

        [HttpGet("db/seed")]
        public void SeedTestData()
        {
            _db.Database.EnsureDeleted();
            _db.Database.EnsureCreated();
        }

        [HttpGet("postman/collection")]
        public string GetPostmanScripts()
        {
            var generator = new PostmanGenerator();
            string content = generator.Generate();
            return content;
        }

        [HttpPost("swagger/import")]
        public void ImportSwagger(ImportSwaggerModel model)
        {
            var importer = new SwaggerImporter();
            importer.Import(model);
        }
    }
}
