﻿using Newtonsoft.Json.Linq;

namespace WebAPI.Swagger
{
    public class SwaggerMethodParameter
    {
        public string Name { get; set; }

        public SwaggerParameterType In { get; set; }

        public bool Required { get; set; }

        public JObject Schema { get; set; }
    }
}
