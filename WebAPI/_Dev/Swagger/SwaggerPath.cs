﻿using System.IO;

namespace WebAPI.Swagger
{
    public class SwaggerPath
    {
        public string Path { get; set; }

        public SwaggerMethod Get { get; set; }

        public SwaggerMethod Post { get; set; }

        public SwaggerMethod Put { get; set; }

        public SwaggerMethod Delete { get; set; }

        public SwaggerMethod Patch { get; set; }

        public void WriteMethods(StreamWriter sw)
        {
            Get?.WriteMethods(sw, Path, "Get");
            Post?.WriteMethods(sw, Path, "Post");
            Put?.WriteMethods(sw, Path, "Put");
            Delete?.WriteMethods(sw, Path, "Delete");
            Patch?.WriteMethods(sw, Path, "Patch");
        }
    }
}
