﻿using Newtonsoft.Json.Linq;

namespace WebAPI.Swagger
{
    public class SwaggerResponse
    {
        public JObject Content { get; set; }

        public string GetResponseType()
        {
            if (Content == null)
            {
                return "void";
            }

            var appJson = Content["application/json"];
            if (appJson != null)
            {
                var schema = appJson["schema"];
                if (schema != null)
                {
                    var paramSchema = SwaggerParameterSchema.FromJToken(schema);
                    string type = paramSchema.LocalType();
                    return type;
                }
            }

            return "IActionResult";
        }
    }
}
