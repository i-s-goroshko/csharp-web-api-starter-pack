﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace WebAPI.Swagger
{
    public class SwaggerParameterSchema
    {
        public string Type { get; set; }

        public string Format { get; set; }

        [JsonProperty("$ref")]
        public string Ref { get; set; }

        public JObject Items { get; set; }

        internal string LocalType()
        {
            if (Type == "array")
            {
                var itemsType = FromJToken(Items);
                string type = itemsType.LocalType();
                return $"IList<{type}>";
            }

            if (Type == "object")
            {
                return "object";
            }

            if (Type == "integer")
            {
                if (Format == "int64")
                {
                    return "long";
                }
                return "int";
            }

            if (Type == "boolean")
            {
                return "bool";
            }

            if (Type == "string")
            {
                if (Format == "date-time")
                {
                    return "DateTime";
                }
                else if (!string.IsNullOrWhiteSpace(Format) && Format != "string")
                {
                    throw new Exception("not supported");
                }
                return "string";
            }

            if (Ref != null)
            {
                var type = Ref.Substring(Ref.LastIndexOf('/') + 1);
                return type;
            }

            if (Items != null)
            {
                var itemsType = FromJToken(Items);
                string type = itemsType.LocalType();
                return type;
            }

            return Type + "." + Format;
        }

        public static SwaggerParameterSchema FromJToken(JToken token)
        {
            var result = token.ToObject<SwaggerParameterSchema>();
            if (result == null && token["$ref"] != null)
            {
                var refValue = token["$ref"].Value<string>();
                if (refValue != null)
                {
                    return new SwaggerParameterSchema
                    {
                        Ref = refValue
                    };
                }
            }

            return result;
        }
    }
}
