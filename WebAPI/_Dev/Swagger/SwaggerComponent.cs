﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace WebAPI.Swagger
{
    public class SwaggerComponent
    {
        public string Name { get; set; }

        public string Type { get; set; }

        public JObject Properties { get; set; }

        public string[] Enum { get; set; }

        public void CreateEntity(string basePath, string nameSpace)
        {
            try
            {
                string filePath = Path.Combine(basePath, Name + ".cs");
                using (var sw = new StreamWriter(filePath))
                {
                    sw.WriteLine("using System;");
                    sw.WriteLine("using System.Collections.Generic;");
                    sw.WriteLine();
                    sw.WriteLine("namespace " + nameSpace);
                    sw.WriteLine("{");
                    sw.WriteLine($"    public {(Enum != null ? "enum" : "class")} " + Name);
                    sw.WriteLine("    {");

                    if (Enum != null)
                    {
                        foreach (var item in Enum)
                        {
                            var comma = item == Enum.Last() ? "" : ",";
                            sw.WriteLine($"        {item}{comma}");
                        }
                    }
                    else if (Properties != null)
                    {
                        foreach (var item in Properties)
                        {
                            var name = item.Key;
                            name = char.ToUpper(name[0]) + name.Substring(1);
                            // if ((item.Value as JToken).Contains()
                            var par = SwaggerParameterSchema.FromJToken(item.Value);
                            if (par == null)
                            {
                                throw new Exception($"Parameter schema not supported: {item.Value.ToString()}.");
                            }
                            sw.WriteLine($"        public {par.LocalType()} {name} {{ get; set; }}");
                        }
                    }

                    sw.WriteLine("    }");
                    sw.WriteLine("}");
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
