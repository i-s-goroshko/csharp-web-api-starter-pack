﻿using System;
using Newtonsoft.Json.Linq;

namespace WebAPI.Swagger
{
    public class SwaggerRequestBody
    {
        public JObject Content { get; set; }

        public string TypeRef
        {
            get
            {
                if (Content == null)
                {
                    return null;
                }

                var contentType = Content["application/json"];
                if (contentType == null)
                {
                    return null;
                }

                var schema = contentType["schema"];
                if (schema == null)
                {
                    return null;
                }

                var refProp = schema["$ref"];
                if (refProp != null)
                {
                    return refProp.ToString();
                }

                var typeProp = schema["type"];
                if (typeProp != null)
                {
                    return typeProp.ToString();
                }

                throw new NotSupportedException("");
            }
        }

        public string TypeRefAlias
        {
            get
            {
                var typeRef = TypeRef;
                if (typeRef == null)
                {
                    return null;
                }

                var type = typeRef.Substring(typeRef.LastIndexOf('/') + 1);
                return type;
            }
        }
    }
}
