﻿using Newtonsoft.Json;

namespace WebAPI.Swagger
{
    public class SwaggerResponses
    {
        [JsonProperty("200")]
        public SwaggerResponse Response200 { get; set; }
    }
}
