﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WebAPI.Swagger
{
    public class SwaggerMethod
    {
        public string Template { get; set; }

        public IList<SwaggerMethodParameter> Parameters { get; set; }

        public SwaggerRequestBody RequestBody { get; set; }

        public SwaggerResponses Responses { get; set; }

        internal void WriteMethods(StreamWriter sw, string path, string method)
        {
            path = path.Trim('\\', '/');
            var parts = path.Split(new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
            var nameParts = parts
                .Where(x => !x.StartsWith("{"))
                .Select(x => char.ToUpper(x[0]) + x.Substring(1));
            var methodName = $"{method}{string.Concat(nameParts)}";

            // return type
            var returnType = Responses?.Response200?.GetResponseType() ?? "IActionResult";

            // body
            var parameters = new List<string>();
            if (RequestBody?.TypeRefAlias != null)
            {
                var alias = RequestBody.TypeRefAlias == "object"
                    ? "dynamic"
                    : RequestBody.TypeRefAlias;
                parameters.Add($"{alias} model");
            }

            // other parameters
            var queryParams = "";
            if (Parameters != null)
            {
                // path params
                foreach (var p in Parameters.Where(x => x.In == SwaggerParameterType.path))
                {
                    var typeSchema = SwaggerParameterSchema.FromJToken(p.Schema);
                    var type = typeSchema.LocalType();
                    parameters.Add($"{type} {p.Name}");
                }

                // query params
                foreach (var p in Parameters.Where(x => x.In == SwaggerParameterType.query))
                {
                    var typeSchema = SwaggerParameterSchema.FromJToken(p.Schema);
                    var type = typeSchema.LocalType();
                    parameters.Add($"{type} {p.Name}");
                    queryParams += $"&{p.Name}={{{p.Name}}}";
                }
                if (queryParams.Length > 0)
                {
                    queryParams = "?" + queryParams.Substring(1);
                }
            }
            var allParams = string.Join(", ", parameters);

            sw.WriteLine();
            sw.WriteLine($"        [Http{method}(\"{path}\")]");
            sw.WriteLine($"        public {returnType} {methodName}({allParams})");
            sw.WriteLine("        {");
            sw.WriteLine("            throw new NotImplementedException();");
            sw.WriteLine("        }");
        }
    }
}
