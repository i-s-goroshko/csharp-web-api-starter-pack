﻿namespace WebAPI.Swagger
{
    public enum SwaggerParameterType
    {
        header,
        path,
        query
    }
}
