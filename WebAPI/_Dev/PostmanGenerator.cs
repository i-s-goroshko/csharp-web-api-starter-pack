﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAPI.Common;
using WebAPI.Controllers;

namespace WebAPI
{
    public class PostmanGenerator
    {
        class PostmanCollectionInfo
        {
            public string Name { get; set; }

            [JsonProperty("_postman_id")]
            public string PostmanId { get; set; } = Guid.NewGuid().ToString().ToLower();

            public string Description { get; set; } = typeof(BaseController).Assembly.FullName;

            public string Schema { get; set; } = "https://schema.getpostman.com/json/collection/v2.1.0/collection.json";
        }

        class ApiHttpRequestHeader
        {
            public string Key { get; set; }

            public string Value { get; set; }

            public string Description { get; set; } = string.Empty;
        }

        class ApiHttpRequest
        {
            public string Url { get; set; }

            public string Method { get; set; }

            public IList<ApiHttpRequestHeader> Header { get; set; } = new List<ApiHttpRequestHeader>();

            public object Body { get; set; } = new object();
        }

        class ApiRequest
        {
            public string Name { get; set; }

            public ApiHttpRequest Request { get; set; } = new ApiHttpRequest();

            public object Response { get; set; } = new object();
        }

        class ApiGroup
        {
            public string Name { get; set; }

            public IList<ApiRequest> Item { get; set; } = new List<ApiRequest>();
        }

        class PostmanCollection
        {
            public object[] Variables { get; set; } = new object[] { };

            public PostmanCollectionInfo Info { get; set; } = new PostmanCollectionInfo();

            public IList<ApiGroup> Item { get; set; } = new List<ApiGroup>();
        }

        public string Generate()
        {
            var contentTypeHeader = new ApiHttpRequestHeader
            {
                Key = "Content-Type",
                Value = "application/json"
            };

            var authorizationHeader = new ApiHttpRequestHeader
            {
                Key = "Authorization",
                Value = "Bearer {{access_token}}"
            };

            var controllerTypes = typeof(BaseController).Assembly.GetExportedTypes().Where(x => x.BaseType == typeof(BaseController));
            var result = new PostmanCollection();
            result.Info.Name = $"WebAPI.Gen.{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}";
            result.Info.Description = "KPI - PerformanceManagement";

            foreach (var controller in controllerTypes)
            {
                var apiGroup = new ApiGroup();
                result.Item.Add(apiGroup);
                apiGroup.Name = controller.Name.Substring(0, controller.Name.Length - "Controller".Length);

                foreach (var method in controller.GetMethods())
                {
                    var apiRequest = new ApiRequest();
                    apiRequest.Request.Method = GetHttpMethod(method, out string path);
                    if (apiRequest.Request.Method != null)
                    {
                        apiGroup.Item.Add(apiRequest);
                        apiRequest.Name = method.Name;
                        apiRequest.Request.Url = "{{url}}/" + char.ToLower(apiGroup.Name[0]) + apiGroup.Name.Substring(1);
                        apiRequest.Request.Header.Add(contentTypeHeader);
                        // apiRequest.Request.Header.Add(authorizationHeader);
                        
                        if (path?.Length > 0)
                        {
                            apiRequest.Request.Url += "/" + path.Replace("{id}", "1");
                        }
                    }
                }
            }

            string content = JsonConvert.SerializeObject(result, Formatting.Indented, Util.SerializerSettings);
            return content;
        }

        public void GenerateAndSaveToFile(string filePath)
        {
            var content = Generate();
            File.WriteAllText(filePath, content);
        }

        private static string GetHttpMethod(MethodInfo method, out string path)
        {
            var get = method.GetCustomAttribute<HttpGetAttribute>();
            if (get != null)
            {
                path = get.Template;
                return "GET";
            }

            var post = method.GetCustomAttribute<HttpPostAttribute>();
            if (post != null)
            {
                path = post.Template;
                return "POST";
            }

            var put = method.GetCustomAttribute<HttpPutAttribute>();
            if (put != null)
            {
                path = put.Template;
                return "PUT";
            }

            var delete = method.GetCustomAttribute<HttpDeleteAttribute>();
            if (delete != null)
            {
                path = delete.Template;
                return "DELETE";
            }

            path = null;
            return null;
        }
    }
}
