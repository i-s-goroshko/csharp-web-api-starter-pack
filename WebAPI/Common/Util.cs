﻿/*
 * Copyright (c) 2019, TopCoder, Inc. All rights reserved.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using WebAPI.Data.Entities;

namespace WebAPI.Common
{
    /// <summary>
    /// This class contains validation methods.
    /// </summary>
    public static class Util
    {
        /// <summary>
        /// Represents the JSON serializer settings.
        /// </summary>
        public static readonly JsonSerializerSettings SerializerSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            DateFormatString = "MM/dd/yyyy HH:mm:ss",
            DateTimeZoneHandling = DateTimeZoneHandling.Utc,
            Converters = new List<JsonConverter> { new StringEnumConverter() }
        };

        /// <summary>
        /// Validates that <paramref name="param"/> is positive number.
        /// </summary>
        ///
        /// <param name="param">The parameter to validate.</param>
        /// <param name="paramName">The name of the parameter.</param>
        ///
        /// <exception cref="ArgumentException">If <paramref name="param"/> is not positive number.</exception>
        public static void ValidateArgumentPositive(long param, string paramName = "param")
        {
            if (param <= 0)
            {
                throw new ArgumentException($"{paramName} should be positive.", paramName);
            }
        }

        /// <summary>
        /// Validates that <paramref name="param"/> is not <c>null</c>.
        /// </summary>
        ///
        /// <typeparam name="T">The type of the parameter, must be reference type.</typeparam>
        ///
        /// <param name="param">The parameter to validate.</param>
        /// <param name="paramName">The name of the parameter.</param>
        ///
        /// <exception cref="ArgumentNullException">If <paramref name="param"/> is <c>null</c>.</exception>
        public static void ValidateArgumentNotNull<T>(T param, string paramName = "param")
            where T : class
        {
            if (param == null)
            {
                throw new ArgumentNullException(paramName, $"{paramName} cannot be null.");
            }
        }

        /// <summary>
        /// Validates that <paramref name="param"/> is not <c>null</c> or empty.
        /// </summary>
        ///
        /// <param name="param">The parameter to validate.</param>
        /// <param name="paramName">The name of the parameter.</param>
        ///
        /// <exception cref="ArgumentNullException">If <paramref name="param"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentException">If <paramref name="param"/> is empty.</exception>
        public static void ValidateArgumentNotNullOrEmpty(string param, string paramName = "param")
        {
            ValidateArgumentNotNull(param, paramName);
            if (string.IsNullOrWhiteSpace(param))
            {
                throw new ArgumentException($"{paramName} cannot be empty.", paramName);
            }
        }

        /// <summary>
        /// Validates that <paramref name="param"/> is not <c>null</c> or empty.
        /// </summary>
        ///
        /// <typeparam name="T">Type of items in collection.</typeparam>
        /// <param name="param">The parameter to validate.</param>
        /// <param name="paramName">The name of the parameter.</param>
        ///
        /// <exception cref="ArgumentNullException">If <paramref name="param"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentException">If <paramref name="param"/> is empty.</exception>
        public static void ValidateArgumentNotNullOrEmpty<T>(ICollection<T> param, string paramName = "param")
        {
            ValidateArgumentNotNull(param, paramName);
            if (param.Count == 0)
            {
                throw new ArgumentException($"{paramName} cannot be empty.", paramName);
            }
        }

        public static void ValidateArgument(bool isValid, string errorMessage, string paramName = null)
        {
            if (!isValid)
            {
                throw new ArgumentException(errorMessage, paramName);
            }
        }

        /// <summary>
        /// Checks whether the given search criteria is <c>null</c> or incorrect.
        /// </summary>
        ///
        /// <param name="criteria">The search criteria to check.</param>
        ///
        /// <exception cref="ArgumentNullException">If the <paramref name="criteria"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentException">If the <paramref name="criteria"/> is incorrect,
        /// e.g. PageNumber is negative, or PageNumber is positive and PageSize is not positive.</exception>
        public static void CheckSearchCriteria(BaseSearchCriteria criteria)
        {
            ValidateArgumentNotNull(criteria, nameof(criteria));

            if (criteria.PageIndex < 0)
            {
                throw new ArgumentException("Page number can't be negative.", nameof(criteria));
            }

            if (criteria.PageIndex > 0 && criteria.PageSize < 1)
            {
                throw new ArgumentException("Page size should be positive, if page number is positive.",
                    nameof(criteria));
            }
        }

        public static int IndexOf<T>(this IEnumerable<T> items, Func<T, bool> predicate)
        {
            int index = 0;
            foreach (var item in items)
            {
                if (predicate(item))
                {
                    return index;
                }
                index++;
            }

            return -1;
        }

        public static void AddRange<T>(this IList<T> list, IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                list.Add(item);
            }
        }

        public static IEnumerable<T> AsNullable<T>(this IEnumerable<T> items)
        {
            return items ?? Enumerable.Empty<T>();
        }

        public static decimal? SumOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector, decimal? defaultValue = null)
        {
            if (source.Any())
            {
                return source.Sum(selector);
            }

            return defaultValue;
        }

        public static bool IsIn(this string value, params string[] values)
        {
            return values.Contains(value);
        }

        public static int GetQuarter(this DateTime date)
        {
            return (date.Month + 2) / 3;
        }

        public static bool IsEmpty(this string value)
        {
            return string.IsNullOrWhiteSpace(value);
        }

        public static T ParseEnum<T>(string value)
            where T : struct
        {
            return Enum.Parse<T>(value, true);
        }
    }
}
